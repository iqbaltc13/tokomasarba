<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth'] ], function() {
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
        Route::match(['get'],'index', 'ProfilController@firstLanding')->name('admin.blank.index');
        Route::group(['prefix' => 'product'], function() {
            Route::match(['post','get'],'store', 'ProdukController@store')->name('admin.product.store');
            Route::match(['post'],'update', 'ProdukController@update')->name('admin.product.update');
            Route::match(['get'],'index', 'ProdukController@index')->name('admin.product.index');
            Route::match(['get'],'data', 'ProdukController@data')->name('admin.product.data');
            Route::match(['get'],'detail', 'ProdukController@detail')->name('admin.product.detail');
            Route::match(['post','get'],'create', 'ProdukController@create')->name('admin.product.create');
            Route::match(['put','post','get'],'edit', 'ProdukController@edit')->name('admin.product.edit');
            Route::match(['post','get'],'delete', 'ProdukController@delete')->name('admin.product.delete');
            Route::match(['get'],'detail-json', 'ProdukController@detailJson')->name('admin.product.detail-json');
    
        });
        Route::group(['prefix' => 'transaction'], function() {
            Route::match(['post','get'],'approve', 'TransaksiController@approve')->name('admin.transaction.approve');
            Route::match(['get','post'],'delete', 'TransaksiController@delete')->name('admin.transaction.delete');
            Route::match(['get'],'index', 'TransaksiController@index')->name('admin.transaction.index');
            Route::match(['get'],'data', 'TransaksiController@data')->name('admin.transaction.data');
            Route::match(['get'],'detail', 'TransaksiController@detail')->name('admin.transaction.detail');
            Route::match(['get'],'detail-json', 'TransaksiController@detailJson')->name('admin.transaction.detail-json');
            Route::match(['post','put','get'],'change-cara-bayar', 'TransaksiController@changeCaraBayar')->name('admin.transaction.changeCaraBayar');
        });
    
        Route::group(['prefix' => 'profil'], function() {
            Route::match(['post','put','get'],'edit', 'ProfilController@edit')->name('admin.profil.edit');
            Route::match(['post','put','get'],'change-password', 'ProfilController@changePassword')->name('admin.profil.change-password');
            Route::match(['get'],'index', 'ProfilController@index')->name('admin.profil.index');
        });
        
    
    });

});
Route::group(['prefix' => 'customer', 'namespace' => 'Customer'], function() {
    Route::group(['prefix' => 'product'], function() {
        Route::match(['get'],'index', 'CustomerController@index')->name('customer.product.index');
        Route::match(['get'],'detail', 'CustomerController@detail')->name('customer.product.detail');
        Route::match(['get'],'detail-json', 'CustomerController@detailJson')->name('customer.product.detail-json');
    });
    Route::group(['prefix' => 'transaction'], function() {
        Route::match(['get'],'create', 'TransaksiController@create')->name('customer.transaction.create');
        Route::match(['post'],'store', 'TransaksiController@store')->name('customer.transaction.store');
        //Route::post('store', 'TransaksiController@store')->name('customer.transaction.store');
        Route::match(['get'],'detail', 'TransaksiController@detail')->name('customer.transaction.detail');
        Route::match(['get'],'create-payment', 'TransaksiController@createPembayaran')->name('customer.transaction.create-payment');
        Route::match(['post'],'store-payment', 'TransaksiController@storePembayaran')->name('customer.transaction.store-payment');
        Route::match(['get'],'view-after-payment', 'TransaksiController@viewAfterPembayaran')->name('customer.transaction.view-after-payment');
    });

});



