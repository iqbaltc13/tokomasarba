@extends('layouts.app_admin')
@section('content')
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Produk</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active" aria-current="page">DataTables</li>
        </ol>
    </div>

          <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                {{-- <h6 class="m-0 font-weight-bold text-primary">DataTables with Hover</h6> --}}
                <a  href="{{route("admin.product.create")}}" class="btn btn-success mb-1">Tambah Produk</a>
            </div>
            <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                <thead class="thead-light">
                    <tr>
                        <th>Id</th>
                        <th>Nama Produk</th>
                        <th>Kategori</th>
                        <th>Harga (Rp)</th>
                        <th>Stok (Unit)</th>
                        <th>Tanggal Input</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Nama Produk</th>
                        <th>Kategori</th>
                        <th>Harga (Rp)</th>
                        <th>Stok (Unit)</th>
                        <th>Tanggal Input</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
                <tbody>
                    
                </tbody>
                </table>
            </div>
            </div>
        </div>
        {{-- modal vertically centered --}}
        <form method="POST" class="form-delete-product" action="{{route("admin.product.delete")}}" enctype=”multipart/form-data”>
            {!! csrf_field() !!}
            <input type="hidden"  class="input-id-delete" name="id" value="0">
        </form>
        <div class="modal fade" id="delete-product-confirmation" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Konfirmasi Hapus Produk </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Anda yakin menghapus produk ini ?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-primary btn-confirm-delete-produk">Iya saya yakin</button>
            </div>
          </div>
        </div>
      </div>

        {{-- modal vertically centerd --}}
        {{-- modal scroled --}}


        <div class="modal fade" id="detail-produk" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalScrollableTitle">Detail Produk</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>
                            <h5>Nama Produk</h5>
                          </td>
                          <td class="nama_produk">
                            
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h5>Kategori</h5>
                          </td>
                          <td class="kategori_produk">
                            
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h5>Stok Tersisa (Unit)</h5>
                          </td>
                          <td class="stok">
                            
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h5>Harga Satuan</h5>
                          </td>
                          <td class="harga">
                            
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h5>Gambar Produk</h5>
                          </td>
                          <td class="gambar_produk">
                            
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h5>Keterangan</h5>
                          </td>
                          <td class="keterangan">
                            
                          </td>
                        </tr>
                        
                        
                      </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
              
            </div>
          </div>
        </div>
      </div>
        {{-- modal scroled--}}
    </div>
</div>
<!--Row-->
@endsection
@push('styles')
<link href="{{url('/')}}/RuangAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="{{url('/')}}/RuangAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/RuangAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        datatable();
        $(document).delegate('.act-hapus','click',function(){
            let idProduk=$(this).attr('data_id');
            $('.input-id-delete').val(idProduk);
            
            // let namaProduk=$(this).attr('nama_produk');
            // order.push(idProduk);
            // nama.push(namaProduk);
            
        });
        $(document).delegate('.act-edit','click',function(){
            let idProduk=$(this).attr('data_id');
            
            let editUrl="{{route('admin.product.edit')}}";
            editUrl=editUrl+'?id='+idProduk;
            window.open(editUrl, '_blank');
            
            
        });
        $(document).delegate('.act-detail','click',function(){
            let idProduk=$(this).attr('data_id');
            $('.input-id-delete').val(idProduk);
            let detailUrl="{{route('admin.product.detail-json')}}";
            detailUrl=detailUrl+'?id='+idProduk;
            getData=$.ajax({
                type: "GET",
                url: detailUrl,
                
                success: function(data)
                {
                    console.log(data);
                    /*
                        nama_produk
                        kategori_produk
                        stok
                        harga
                        keterangan
                    harga='<h5>Rp '+data.data.harga+',-</h5>';
                    */
                    nama_produk='<h5>'+data.data.nama+'</h5>';
                    $('.nama_produk').html(nama_produk);
                    kategori='<h5>'+data.data.kategori+'</h5>';
                    $('.kategori_produk').html(kategori);
                    keterangan='<h5>'+data.data.keterangan+'</h5>';
                    $('.keterangan').html(keterangan);
                    stok='<h5>'+data.data.quantity+'</h5>';
                    $('.stok').html(stok);
                    harga='<h5>Rp '+data.data.harga+',-</h5>';
                    $('.harga').html(harga);
                    base_url="{{url('/')}}";
                    base_url_image = base_url + '/aranoz/img/product/';
                    
                    if(data.data.gambar_produk.length > 0){
                        gambar='<img src="'+ base_url_image+data.data.gambar_produk[0].link_file+'" alt="" height="90" width="60">';
                        $('.gambar_produk').html(gambar);
                    }
                }
            });


            // let namaProduk=$(this).attr('nama_produk');
            // order.push(idProduk);
            // nama.push(namaProduk);
            
        });
        //btn-confirm-delete-produk
        $(document).delegate('.btn-confirm-delete-produk','click',function(){
            $('.form-delete-product').submit();
            $("#delete-product-confirmation").modal("hide");
        });
    });
</script>
<script>
    function datatable(){
        var url = "{{route('admin.product.data')}}";
        var table = $('#dataTableHover').DataTable({
            // ordering: false,
            columnDefs: [
                            
                            { "orderable": false, "targets": 5 },
                            { "orderable": false, "targets": 6 },
                            {
                                    "targets": '_all',
                                    "defaultContent": "---"
                            },
                        ],
            order: [[ 0, "desc" ]],
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [
                { 
                        data: 'id',
                    },
                    {
                        data: 'nama',
                        

                    },
                    {
                        data: 'kategori',
                        

                    },
                    {
                        data: 'harga',
                        

                    },
                   
                    {
                        data: 'quantity',
                        

                    },
                    {
                        data: 'created_at',
                        searchable:false,
                        // render:function(data){
                        //     var status="";
                        //     if(data.status3=="Belum Disetujui"){
                        //         status+='<span class="label label-warning">'+data.status3+'</span>';
                        //     }
                        //     if(data.status3=="Sudah Disetujui"){
                        //         status+='<span class="label label-success">'+data.status3+'</span>';
                        //     }
                        //     if(data.status3=="Ditolak"){
                        //         status+='<span class="label label-danger">'+data.status3+'</span>';
                        //     }
                        //     return status;
                            
                        // }
                        

                    },
                    
                    { 
                        data: null,
                        searchable: false,
                        render: function(data){
                            aksi='<a data_id='+data.id+ 'href="javascript:void(0)" data-toggle="modal" data-target="#detail-produk" class="btn btn-outline-primary mb-1 act-detail">Detail</a>&nbsp;'+
                                 '<a data_id='+data.id+ 'href="javascript:void(0)" class="btn btn-outline-secondary mb-1 act-edit">Edit</a>&nbsp;'+
                                 '<a data_id='+data.id+ 'href="javascript:void(0)" data-toggle="modal" data-target="#delete-product-confirmation" class="btn btn-outline-danger mb-1 act-hapus">Hapus</a>';
                            return aksi;
                        }
                        
                    },
                    
                    
                    
                ]
        });
    }
</script>

@endpush