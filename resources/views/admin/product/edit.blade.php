@extends('layouts.app_admin')
@section('content')
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Produk</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Form Edit Produk</li>
      </ol>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card mb-8">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                {{-- <h6 class="m-0 font-weight-bold text-primary">Form Edit Produk</h6> --}}
                <a  href="{{route("admin.product.index")}}" class="btn btn-success mb-1 ">List Produk</a>
                </div>
                <div class="card-body">
                    <form method="POST" class="form-update-product" action="{{route("admin.product.update")}}" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="nama_produk">Nama Produk</label>
                            <input type="text" class="form-control" name="nama" id="nama_produk" aria-describedby="nama_produk"
                        placeholder="Masukkan Nama Produk" value="{{$produks->nama}}" required >
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your
                                email with anyone else.</small> --}}
                        </div>
                        <input type="hidden"  name="id" value="{{$produks->id}}">
                        <div class="form-group">
                            <label for="kategori">Kategori</label>
                            <input type="text" class="form-control" name="kategori" id="kategori" aria-describedby="kategori"
                                placeholder="Masukkan Kategori Produk" value="{{$produks->kategori}}" required>
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your
                                email with anyone else.</small> --}}
                        </div>
                        <div class="form-group">
                            <label for="kategori">Harga Satuan</label>
                            
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input type="number"  name="harga"  class="form-control" aria-label="" value="{{$produks->harga}}" required>
                            <div class="input-group-append">
                            <span class="input-group-text">,-</span>
                            </div>
                           
                        </div>
                        <div class="form-group">
                            <label for="kategori">Jumlah Barang</label>
                            
                        </div>
                        <div class="input-group mb-3">
                           
                            <input type="number"  name="quantity"  class="form-control" aria-label="" value="{{$produks->quantity}}" required>
                            <div class="input-group-append">
                            <span class="input-group-text">Unit</span>
                            </div>
                           
                        </div>
                        <div class="form-group">
                            @if(sizeof($produks->gambarProduk)>0)
                            <label for="gambar_produk_current">Gambar Produk Saat ini</label>
                            
                            @else
                            <label for="gambar_produk_current">Tidak ada Gambar Produk Saat ini</label>
                            @endif
                            
                        </div>
                        @if(sizeof($produks->gambarProduk)>0)
                        <div class="form-group">
                            <img src="{{url('/')}}/aranoz/img/product/{{$produks->gambarProduk[0]->link_file}}" alt="" width="100" height="150">
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="gambar_produ">Gambar Produk</label>
                            
                        </div>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" name="gambar_produk[]"  id="gambar_produk" >
                            
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan/Deskripsi Produk</label>
                            <textarea class="form-control" id="keterangan" name="keterangan" rows="5" value="{{$produks->keterangan}}" required>{{$produks->keterangan}}</textarea>
                        </div>
                        {{-- <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                <label class="custom-control-label" for="customControlAutosizing">Remember me</label>
                            </div>
                        </div> --}}
                        
                </form>
                <button type="submit" data-toggle="modal" data-target="#edit-product-confirmation" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit-product-confirmation" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Konfirmasi Edit Produk?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Anda yakin dengan data yang anda isi ?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-primary btn-confirm-edit-produk">Iya saya yakin</button>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
@push('styles')
@endpush

@push('scripts')
<script>
    $(document).ready(function(){
        $(document).delegate('.btn-confirm-edit-produk','click',function(){
            $('.form-update-product').submit();
            $("#edit-product-confirmation").modal("hide");
        });
    });
</script>
@endpush

