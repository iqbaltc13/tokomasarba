@extends('layouts.app_admin')
@section('content')
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Transaksi</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active" aria-current="page">DataTables</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="card mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Detail Transaksi</h6>
                  </div>
                  <div class="card-body">
                    <form>
                      <div class="form-group">
                        <label for="nama_pembeli">Nama Pembeli</label>
                      <input type="text" class="form-control" id="nama_pembeli" value="{{$transaksis->nama_pembeli}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="no_ktp_pembeli">No KTP Pembeli</label>
                        <input type="text" class="form-control" id="no_ktp_pembeli" value="{{$transaksis->no_ktp_pembeli}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="email_pembeli">Email Pembeli</label>
                        <input type="email" class="form-control" id="email_pembeli" value="{{$transaksis->email_pembeli}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="alamat_pembeli">Alamat Pembeli</label>
                        <input type="text" class="form-control" id="alamat_pembeli" value="{{$transaksis->alamat_pembeli}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="harga_tertagih">Harga Tertagih</label>
                        <input type="text" class="form-control" id="harga_tertagih" value="Rp {{$transaksis->harga_tertagih}},-" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="kode_transaksi">Kode Transaksi</label>
                        <input type="text" class="form-control" id="kode_transaksi" value="{{$transaksis->kode_transaksi}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="status">Status</label>
                        @if($transaksis->status==1)
                            @php
                                $status='Sudah Disetujui';
                            @endphp
                        @else
                            @php
                                $status='Belum Disetujui';
                            @endphp
                        @endif
                      <input type="text" class="form-control" id="status" value="{{$status}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="status_pembayaran">Status Pembayaran</label>
                        @if($transaksis->pembayaran)
                            @php
                                $statusBayar='Sudah dilakukan pembayaran';
                            @endphp
                        @else
                            @php
                                $statusBayar='Belum dilakukan pembayaran';
                            @endphp
                        @endif
                        <input type="text" class="form-control" id="status_pembayaran" value="{{$statusBayar}}" readonly>
                        
                      </div>
                      
                      
                    </form>
                    
                  </div>
                  @if($transaksis->pembayaran)
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Detail Pembayaran</h6>
                  </div>
                  <div class="card-body">
                    <form>
                      <div class="form-group">
                        <label for="nama_bank">Nama Bank</label>
                        <input type="text" class="form-control" id="nama_bank" value="{{$transaksis->pembayaran->nama_bank}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="no_rekening">No Rekening</label>
                        <input type="text" class="form-control" id="no_rekening" value="{{$transaksis->pembayaran->no_rekening}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="rekening_atas_nama">Rekening Atas Nama</label>
                        <input type="text" class="form-control" id="rekening_atas_nama" value="{{$transaksis->pembayaran->rekening_atas_nama}}" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="jumlah_yang_ditransfer">Jumlah yang dibayarkan</label>
                        <input type="text" class="form-control" id="jumlah_yang_ditransfer" value="Rp {{$transaksis->pembayaran->jumlah_yang_ditransfer}},-" readonly>
                        
                      </div>
                      <div class="form-group">
                        <label for="tanggal_transfer">Tanggal Transfer</label>
                        <input type="text" class="form-control" id="tanggal_transfer" value="{{$transaksis->pembayaran->tanggal_transfer}}" readonly>
                        
                      </div>
                    </form>
                  </div>
                  @endif
                  <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                        <tr>
                            <th>No</th>
                            <th>Nama Produk</th>
                            <th>Kategori</th>
                            <th>Harga Satuan (Rp)</th>
                            
                            <th>Stok (Unit)</th>
                            <th>Harga Total (Rp)</th>
                        </tr>
                    </thead>
                    @foreach($transaksis->produk as $produk)
                        <tr>
                          <td>{{$produk->id}}</td>
                          <td>{{$produk->produk->nama}}</td>
                          
                          <td>{{$produk->produk->kategori}}</td>
                          <td>{{$produk->produk->harga}}</td>
                          <td>{{$produk->jumlah_barang}}</td>
                          @php
                              $hargaTotal=0;
                              $hargaTotal=$produk->produk->harga * $produk->jumlah_barang;
                          @endphp
                          <td>{{$hargaTotal}}</td>
                        
                        </tr>
                    @endforeach
                    <tbody>
                        
                    </tbody>
                    </table>
                  </div>
                
            </div>
        </div>
    </div>
</div>

@endsection
@push('styles')
@endpush
@push('scripts')
@endpush