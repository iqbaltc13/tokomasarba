@extends('layouts.app_admin')
@section('content')
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Transaksi</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active" aria-current="page">DataTables</li>
        </ol>
    </div>

          <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                {{-- <h6 class="m-0 font-weight-bold text-primary">DataTables with Hover</h6> --}}
            </div>
            <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                <thead class="thead-light">
                    <tr>
                        <th>Id</th>
                        <th>Nama Pembeli</th>
                        <th>No KTP Pembeli</th>
                        <th>No HP Pembeli</th>
                        <th>Alamat Pembeli</th>
                        <th>Email Pembeli</th>
                        <th>Harga Tertagih (Rp)</th>
                        <th>Tanggal Transaksi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Nama Pembeli</th>
                        <th>No KTP Pembeli</th>
                        <th>No HP Pembeli</th>
                        <th>Alamat Pembeli</th>
                        <th>Email Pembeli</th>
                        <th>Harga Tertagih (Rp)</th>
                        <th>Tanggal Transaksi</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
                <tbody>
                    
                </tbody>
                </table>
            </div>
            </div>
        </div>
        <form method="POST" class="form-delete-transaksi" action="{{route("admin.transaction.delete")}}" enctype=”multipart/form-data”>
            {!! csrf_field() !!}
            <input type="hidden"  class="input-id-delete" name="id" value="0">
        </form>
        <form method="POST" class="form-approve-transaksi" action="{{route("admin.transaction.approve")}}" enctype=”multipart/form-data”>
            {!! csrf_field() !!}
            <input type="hidden"  class="input-id-approve" name="id_approve" value="0">
        </form>
        <div class="modal fade" id="delete-transaksi-confirmation" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Konfirmasi Hapus Transaksi </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Anda yakin menghapus transaksi ini ?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-primary btn-confirm-delete-transaksi">Iya saya yakin</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="approve-transaksi-confirmation" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Konfirmasi Approve Transaksi </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Anda yakin menyetujui transaksi ini ?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-primary btn-confirm-approve-transaksi">Iya saya yakin</button>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!--Row-->
@endsection
@push('styles')
<link href="{{url('/')}}/RuangAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="{{url('/')}}/RuangAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/RuangAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        datatable();
        $(document).delegate('.act-approve','click',function(){
            let idProduk=$(this).attr('data_id');
            $('.input-id-approve').val(idProduk);
            
            // let namaProduk=$(this).attr('nama_produk');
            // order.push(idProduk);
            // nama.push(namaProduk);
            
        });
        $(document).delegate('.btn-confirm-approve-transaksi','click',function(){
            $('.form-approve-transaksi').submit();
            $("#approve-transaksi-confirmation").modal("hide");
        });
        $(document).delegate('.act-hapus','click',function(){
            let idProduk=$(this).attr('data_id');
            $('.input-id-delete').val(idProduk);
            
            // let namaProduk=$(this).attr('nama_produk');
            // order.push(idProduk);
            // nama.push(namaProduk);
            
        });
        $(document).delegate('.btn-confirm-delete-transaksi','click',function(){
            $('.form-delete-transaksi').submit();
            $("#delete-transaksi-confirmation").modal("hide");
        });
        $(document).delegate('.act-detail','click',function(){
            let idProduk=$(this).attr('data_id');
            url="{{route('admin.transaction.detail')}}";
            url=url +'?id='+idProduk;
            window.open(url, '_blank');
            
            // let namaProduk=$(this).attr('nama_produk');
            // order.push(idProduk);
            // nama.push(namaProduk);
            
        });
    });
</script>
<script>
    function datatable(){
        var url = "{{route('admin.transaction.data')}}";
        var table = $('#dataTableHover').DataTable({
            // ordering: false,
            columnDefs: [
                            
                            { "orderable": false, "targets": 5 },
                            { "orderable": false, "targets": 6 },
                            {
                                    "targets": '_all',
                                    "defaultContent": "---"
                            },
                        ],
            order: [[ 0, "desc" ]],
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [
                { 
                        data: 'id',
                    },
                    {
                        data: 'nama_pembeli',
                        

                    },
                    {
                        data: 'no_ktp_pembeli',
                        

                    },
                    {
                        data: 'no_hp_pembeli',
                        

                    },
                    {
                        data: 'alamat_pembeli',
                        

                    },
                   
                    {
                        data: 'email_pembeli',
                        

                    },
                    {
                        data: 'harga_tertagih',
                        

                    },
                    {
                        data: 'created_at',
                        searchable:false,
                        // render:function(data){
                        //     var status="";
                        //     if(data.status3=="Belum Disetujui"){
                        //         status+='<span class="label label-warning">'+data.status3+'</span>';
                        //     }
                        //     if(data.status3=="Sudah Disetujui"){
                        //         status+='<span class="label label-success">'+data.status3+'</span>';
                        //     }
                        //     if(data.status3=="Ditolak"){
                        //         status+='<span class="label label-danger">'+data.status3+'</span>';
                        //     }
                        //     return status;
                            
                        // }
                        

                    },
                    
                    { 
                        data: null,
                        searchable: false,
                        render: function(data){
                            aksi='<a data_id='+data.id+ 'href="javascript:void(0)"  class="btn btn-outline-primary mb-1 act-detail">Detail</a>&nbsp;';
                                  if(data.status.includes("0")){
                                    aksi += '<a data_id='+data.id+ 'href="javascript:void(0)" data-toggle="modal" data-target="#approve-transaksi-confirmation" class="btn btn-outline-secondary mb-1 act-approve">Approve</a>&nbsp;'+
                                            '<a data_id='+data.id+ 'href="javascript:void(0)" data-toggle="modal" data-target="#delete-transaksi-confirmation" class="btn btn-outline-danger mb-1 act-hapus">Hapus</a>';
                                  }
                                 
                                
                            return aksi;
                        }
                        
                    },
                    
                    
                    
                ]
        });
    }
</script>

@endpush