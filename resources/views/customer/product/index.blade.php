@extends('layouts.app_customer')
@section('content')
<div id="vueelement">
<section class="product_list section_padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section_tittle text-center">
                    <h2>awesome <span> <a class="btn btn-light proses-pesanan">Proses Pesanan</a> </span> </h2>
                   
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="product_list_slider owl-carousel">
                    <div class="single_product_list_slider">
                        <div class="row align-items-center justify-content-between">
                            @php
                                $batas=sizeof($produks);
                                if(sizeof($produks)>8){
                                    $batas=8;
                                }
                                $pagination=sizeof($produks)/8-1;
                                if($pagination <= 0){
                                    $totalPage=0;
                                }
                                else{
                                    $totalPage=$pagination;
                                }
                                $sisa=sizeof($produks)%8;
                                if($sisa > 0){
                                    $totalPage=$totalPage + 1;
                                }
                                
                            @endphp
                            @for($i=0;$i<$batas;$i++)
                            @if(isset($produks[$i]))
                            <div class="col-lg-3 col-sm-6">
                                <div class="single_product_item">
                                @if(sizeof($produks[$i]->gambarProduk)>0)
                                <img src="{{url('/')}}/aranoz/img/product/{{$produks[$i]->gambarProduk[0]->link_file}}" alt="">
                                @endif
                                    <div class="single_product_text">
                                        <h4>{{$produks[$i]->nama}}</h4>
                                        <h3>Rp {{$produks[$i]->harga}},-</h3>
                                        <a id_produk="{{ $produks[$i]->id }}" nama_produk="{{ $produks[$i]->nama }}" href="javascript:void(0)" class="add_cart">+ add to cart<i class="ti-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endfor
                            
                        </div>
                    </div>
                   @php
                      $minusNoSlider=sizeof($produks)-8;
                      $totalSlider=0;
                      $modWithEight=$minusNoSlider%8;
                      if($minusNoSlider > 0){
                          $minusByModWithEight=$minusNoSlider-$modWithEight;
                          $totalSlider=$minusByModWithEight/8;
                          if($modWithEight != 0){
                              $totalSlider=$totalSlider+1;
                          }
                          
                      }
                      
                   @endphp 
                   @if($minusNoSlider > 0)
                    @for($i=0;$i<$totalSlider;$i++)
                    <div class="single_product_list_slider">
                        <div class="row align-items-center justify-content-between">
                            @php
                              $page=$i+1;
                            @endphp
                            @for($j=0;$j<8;$j++)
                            @php
                              $indeks=$page*8+$j;  
                            @endphp
                            @if(isset($produks[$indeks]))
                            <div class="col-lg-3 col-sm-6">
                                <div class="single_product_item">
                                    @if(sizeof($produks[$indeks]->gambarProduk)>0)
                                    <img src="{{url('/')}}/aranoz/img/product/{{$produks[$indeks]->gambarProduk[0]->link_file}}" alt="">
                                    @endif
                                        <div class="single_product_text">
                                            <h4>{{$produks[$indeks]->nama}}</h4>
                                            <h3>Rp {{$produks[$indeks]->harga}},-</h3>
                                            {{-- <a v-bind:id_produk='{!! json_encode($produks[$indeks]->id) !!}' v-bind:nama_produk='{!! json_encode($produks[$indeks]->nama) !!}' href="javascript:void(0)" class="add_cart">+ add to cart<i class="ti-heart"></i></a> --}}
                                            <a id_produk="{{ $produks[$indeks]->id }}" nama_produk="{{ $produks[$indeks]->nama }}" href="javascript:void(0)" class="add_cart">+ add to cart<i class="ti-heart"></i></a>
                                        </div>
                                </div>
                            </div>
                            @endif
                            @endfor
                            
                        </div>
                        
                    </div>
                    @endfor
                @endif
                   
                    <form method="GET" class="form-create-transaction" action="{{route("customer.transaction.create")}}" enctype=”multipart/form-data”>
                        {!! csrf_field() !!}
                    </form>
                        
                
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script>
        let order=[];
        let nama=[];
    $('.add_cart').click(function(){
        let idProduk=$(this).attr('id_produk');
        let namaProduk=$(this).attr('nama_produk');
        order.push(idProduk);
        nama.push(namaProduk);
        
    });
    $('.proses-pesanan').click(function(){
        if(nama.length <= 0){

            alert('anda belum memilih produk yang akan dipesan');
        }
        else{
            text='';
            $.each(order, function( index, value ) {
                // urut = index + 1;
                // text +=urut + ": " + value + " \n";
                form='';
                form += '<input type="hidden"  name="id_produk[]" value="'+value+'">';
                	
                $( ".form-create-transaction" ).append(form);


            });
            $( ".form-create-transaction" ).submit();
            
                
        }
    });
    // new Vue({
    //         el: '#vueelement',
    //         data:{
               
    //             order:[],
    //             nama:[],
               
    //         },
    //         watch:{
               
    //         },
    //         methods:{
    //             // indexCounter:function(a){
    //             //         ++a;
    //             //      this.index=a;
    //             //      console.log(this.index);
    //             //      return this.index;
    //             // },
                
                
    //         },
    //         computed:{
    //             // indexCounter:function(){
    //             //      return ++this.index;   
    //             // }
    //         },
    //         mounted() {
    //             //axios.get(link).then(response => console.log(response.data));
    //             //axios.get(link).then(response => this.ds=response.data);
    //         },
    // });
    </script>
@endpush