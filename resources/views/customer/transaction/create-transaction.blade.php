@extends('layouts.app_customer')
@section('content')
<br/>
<br/>
<br/>
<br/>
<section class="cart_area padding_top">
    <div class="container">
      <div class="cart_inner">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Produk</th>
                <th scope="col">Harga</th>
                <th scope="col">Jumlah (Unit)</th>
                <th scope="col">Total Harga</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($produks as $produk)
               
              <tr>
                <td>
                  <div class="media">
                    <div class="d-flex">
                        @if(sizeof($produk->gambarProduk)>0)
                       
                            <img src="{{url('/')}}/aranoz/img/product/{{$produk->gambarProduk[0]->link_file}}" alt="" height="120" width="100" />
                        @endif
                    </div>
                    <div class="media-body">
                        <p>{{$produk->nama}}</p>
                    </div>
                  </div>
                </td>
                <td>
                  <h5>Rp {{$produk->harga}},-</h5>
                </td>
                <td>
                  <div class="product_count">
                    <span class="input-number-decrement" id_produk="{{$produk->id}}" harga="{{$produk->harga}}"> <i class="ti-angle-down"></i></span>
                    <input class="input-number-{{$produk->id}}" id_produk="{{$produk->id}}" type="text" value="1" min="1" max="{{$produk->quantity}}">
                    <span class="input-number-increment" id_produk="{{$produk->id}}" harga="{{$produk->harga}}"> <i class="ti-angle-up"></i></span>
                  </div>
                </td>
                <td class="td-total-harga-{{$produk->id}}">
                  <h5 class="total-harga-{{$produk->id}}">Rp {{$produk->harga}},-</h5>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{-- <div class="checkout_btn_inner float-right">
            <a class="btn_1" href="#">Continue Shopping</a>
            <a class="btn_1 checkout_btn_1" href="#">Proceed to checkout</a>
          </div> --}}
        </div>
    </div>
</section>
<div class="section-top-border">
    <div class="row">
        <div class="col-lg-12">
            <center><h3 class="mb-30">Isi Form Transaksi Berikut</h3></center>
        <form method="POST" id="transaction-store" action="{{route("customer.transaction.store")}}" enctype=”multipart/form-data”>
                 {!! csrf_field() !!}
                 @php
                    $totalHarga=0;
                @endphp
                @foreach ($produks as $produk)
                
                <input type="hidden"  name="id_produk[]" value="{{$produk->id}}">
                <input type="hidden"  name="harga_satuan_produk[]" value="{{$produk->harga}}">
                <input type="hidden" class="quantity-id-{{$produk->id}}" name="quantity_produk[]" value="1">
                <input type="hidden" class="harga-total-id-{{$produk->id}}" name="harga_total_produk[]" value="{{$produk->harga}}">
                @php
                    $totalHarga=$totalHarga+$produk->harga;
                @endphp
                @endforeach
                <input type="hidden"  class="harga-total-all" name="harga_tertagih" value="{{$totalHarga}}">
                        
                <div class="mt-10">
                    <input type="text" name="nama_pembeli" placeholder="Nama Pembeli"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Pembeli'" required
                        class="single-input">
                </div>
                <div class="mt-10">
                    <input type="text" name="no_ktp_pembeli" placeholder="No KTP Pembeli" onfocus="this.placeholder = ''"
                        onblur="this.placeholder = 'No KTP Pembeli'" required class="single-input" required>
                </div>
                <div class="mt-10">
                    <input type="text" name="no_hp_pembeli" placeholder="No HP Pembeli" onfocus="this.placeholder = ''"
                        onblur="this.placeholder = 'No HP Pembeli'" required class="single-input" required>
                </div>
                <div class="mt-10">
                  <input type="text" name="alamat_pembeli" placeholder="Alamat Pembeli" onfocus="this.placeholder = ''"
                      onblur="this.placeholder = 'Alamat Pembeli'" required class="single-input" required>
              </div>
                <div class="mt-10">
                    <input type="email" name="email_pembeli" placeholder="Email Pembeli" onfocus="this.placeholder = ''"
                        onblur="this.placeholder = 'Email Pembeli'" required class="single-input" required>
                </div>
                <br/>
                <br/>
                <div class="col-md-4">
                </div>
                <center>
                <div class="col-md-4">
                    <button type="submit" class="genric-btn success-border">Submit </button>
                </div>
                </center>
               
                
            </form>
        </div>
        
    </div>
</div>
@endsection
@push('styles')
@endpush
@push('scripts')

    <script>
        let total_harga_all="{{$totalHarga}}";
        $(".input-number-decrement").click(function(){
            id_produk=$(this).attr("id_produk");
            harga_satuan=$(this).attr("harga");
            min_quantity=$('.input-number-'+id_produk).attr('min') *1;
            cur_quantity=$('.input-number-'+id_produk).val() * 1;
            updated_quantity=cur_quantity-1;
            devisit_total=harga_satuan;
            if(updated_quantity < min_quantity){
                updated_quantity=min_quantity;
                devisit_total=0;
            }
            count_total_harga=harga_satuan * updated_quantity;
            str_total_harga='<h5 class="total-harga-'+id_produk+'">Rp '+count_total_harga+',-</h5>';
            $('.total-harga-'+id_produk).remove();
            $('.td-total-harga-'+id_produk).html(str_total_harga);
            
            $('.input-number-'+id_produk).val(updated_quantity);
            $('.quantity-id-'+id_produk).val(updated_quantity);
            $('.quantity-id-'+id_produk).val(updated_quantity);
            $('.harga-total-id-'+id_produk).val(count_total_harga);
            total_harga_all=total_harga_all*1;
            // total_harga_all=total_harga_all-devisit_total;
            // $('.harga-total-all').val(total_harga_all);
            getTotalHarga();
        });
        $(".input-number-increment").click(function(){
            id_produk=$(this).attr("id_produk");
            harga_satuan=$(this).attr("harga");
            max_quantity=$('.input-number-'+id_produk).attr('max') *1;
            cur_quantity=$('.input-number-'+id_produk).val() * 1;
            updated_quantity=cur_quantity+1;
            tambahan_total=harga_satuan;
            if(updated_quantity > max_quantity){
                updated_quantity=max_quantity;
                tambahan_total=0;
            }
            count_total_harga=harga_satuan * updated_quantity;
            str_total_harga='<h5 class="total-harga-'+id_produk+'">Rp '+count_total_harga+',-</h5>';

            $('.total-harga-'+id_produk).remove();
            $('.td-total-harga-'+id_produk).html(str_total_harga);
            $('.input-number-'+id_produk).val(updated_quantity);
            $('.quantity-id-'+id_produk).val(updated_quantity);
            $('.harga-total-id-'+id_produk).val(count_total_harga);
            total_harga_all=total_harga_all*1;
            // total_harga_all=total_harga_all+tambahan_total;
            // $('.harga-total-all').val(total_harga_all);
            getTotalHarga();
        });
    </script>
    <script>
      function getTotalHarga(){
        arr_total_harga= $('input[name="harga_total_produk[]"]').map(function () {
          return this.value; // $(this).val()
        }).get();
        total=0;
        arr_total_harga.forEach(function(value) {
          value=parseInt(value)* 1;
          total += value;
        });
        //console.log(total);
        $('.harga-total-all').val(total);
        
      }
    </script>

@endpush