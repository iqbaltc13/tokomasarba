@extends('layouts.app_customer')
@section('content')
<br/>
<br/>
<br/>
<br/>
<section class="product_description_area">
    <div class="container">
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment"
              aria-selected="false">Formulir Pembayaran</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
            aria-selected="false">Data Pembeli</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review"
            aria-selected="false">Data Barang</a>
        </li>
        
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="payment" role="tabpanel" aria-labelledby="payment-tab">
            <div class="row">
                <div class="col-lg-12">
                    <center><h3 class="mb-30">Isi Form Transaksi Berikut</h3></center>
                    <form method="POST" id="payment-store" action="{{route("customer.transaction.store-payment")}}" enctype=”multipart/form-data”>
                        {!! csrf_field() !!}
                        <input type="hidden"  name="id_transaksi" value="{{$transaksis->id}}">
                        <div class="mt-10">
                            <input type="text" name="nama_bank" placeholder="Nama Bank"
                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Bank'" required
                            class="single-input">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="no_rekening" placeholder="Nomor Rekening"
                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nomor Rekening'" required
                            class="single-input">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="rekening_atas_nama" placeholder="Nama Rekening"
                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Rekening'" required
                            class="single-input">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="jumlah_yang_ditransfer" placeholder="Jumlah Uang yang Ditransfer"
                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Jumlah Uang yang Ditransfer'" required
                            class="single-input">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="tanggal_transfer" placeholder="Tanggal Transfer"
                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tanggal Transfer'" required
                            class="single-input">
                        </div>
                        <br/>
                        <br/>
                        <div class="col-md-4">
                        </div>
                        <center>
                        <div class="col-md-4">
                            <button type="submit" class="genric-btn success-border">Submit </button>
                        </div>
                        </center>
                    </form>
                </div>
                
            </div>
        </div>
        <div class="tab-pane fade show " id="profile" role="tabpanel" aria-labelledby="profile-tab">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <td>
                    <h5>Nama Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->nama_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>No KTP Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->no_ktp_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>No HP Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->no_hp_pembeli}}/h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Email Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->email_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Alamat Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->alamat_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Harga Tertagih</h5>
                  </td>
                  <td>
                    <h5>Rp {{$transaksis->harga_tertagih}},-</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Kode Transaksi</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->kode_transaksi}}</h5>
                  </td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
        <div class="tab-pane fade " id="review" role="tabpanel" aria-labelledby="review-tab">
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Produk</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Jumlah (Unit)</th>
                    <th scope="col">Total Harga</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($transaksis->produk as $produk)
                  @if($produk->produk)
                  @php
                   $totalHarga=0;
                   $totalHarga= $produk->jumlah_barang  * $produk->produk->harga;
                  @endphp
                  <tr>
                    <td>
                      <div class="media">
                        <div class="d-flex">
                            @if(sizeof($produk->produk->gambarProduk)>0)
                           
                                <img src="{{url('/')}}/aranoz/img/product/{{$produk->produk->gambarProduk[0]->link_file}}" alt="" height="120" width="100" />
                            @endif
                        </div>
                        <div class="media-body">
                            <p>{{$produk->produk->nama}}</p>
                        </div>
                      </div>
                    </td>
                    <td>
                      <h5>Rp {{$produk->produk->harga}},-</h5>
                      
                    </td>
                    <td>
                      <div class="product_count">
                        <h5> {{$produk->jumlah_barang}}</h5>
                      </div>
                    </td>
                    <td class="td-total-harga-{{$produk->id_barang}}">
                      <h5 class="total-harga-{{$produk->id_barang}}">Rp {{$totalHarga}},-</h5>
                    </td>
                  </tr>
                  @endif
                  @endforeach
                </tbody>
              </table>
              {{-- <div class="checkout_btn_inner float-right">
                <a class="btn_1" href="#">Continue Shopping</a>
                <a class="btn_1 checkout_btn_1" href="#">Proceed to checkout</a>
              </div> --}}
            </div>
      </div>
        
        
      </div>
    </div>
    {{-- <center>
      <div class="col-md-4">
        <a href="{{route('customer.transaction.create-payment').'?id='.$transaksis->kode_transaksi}}" class="genric-btn primary e-large">Lanjut Ke Pembayaran</a>
      </div>
    </center> --}}
    
    
  </section>
@endsection
@push('styles')
<link rel="stylesheet" href="{{url('/')}}/aranoz/css/lightslider.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">

    <!-- Dropzone Css -->
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/dropzone/dropzone.css">

    <!-- Multi Select Css -->
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/multi-select/css/multi-select.css">

    <!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/jquery-spinner/css/bootstrap-spinner.css">

    <!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

    <!-- Bootstrap Select Css -->
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/bootstrap-select/css/bootstrap-select.css">

    <!-- noUISlider Css -->
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/nouislider/nouislider.min.css">
    
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/node-waves/waves.css">
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/animate-css/animate.css">
<link rel="stylesheet" href="{{url('/')}}/bsbmd/plugins/morrisjs/morris.css">
@endpush
@push('scripts')

  <script src="{{url('/')}}/aranoz/js/lightslider.min.js"></script>
  <script src="{{url('/')}}/aranoz/js/owl.carousel.min.js"></script>
  <script src="{{url('/')}}/aranoz/js/jquery.nice-select.min.js"></script>
  <script src="{{url('/')}}/aranoz/js/stellar.js"></script>
  
  <script src="{{url('/')}}/aranoz/js/theme.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/momentjs/moment.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/chartjs/Chart.bundle.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/flot-charts/jquery.flot.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/flot-charts/jquery.flot.resize.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/flot-charts/jquery.flot.pie.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/flot-charts/jquery.flot.categories.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/flot-charts/jquery.flot.time.js"></script>
  <script src="{{url('/')}}/bsbmd/plugins/jquery-sparkline/jquery.sparkline.js"></script>
  <script>
    $('input[name="tanggal_transfer"]').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false,format : 'YYYY-MM-DD'});
  </script>
@endpush