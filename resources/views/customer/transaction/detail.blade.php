@extends('layouts.app_customer')
@section('content')
<br/>
<br/>
<br/>
<br/>
<section class="product_description_area">
    <div class="container">
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        
        
        <li class="nav-item">
          <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
            aria-selected="false">Data Pembeli</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review"
            aria-selected="false">Data Barang</a>
        </li>
        
      </ul>
      <div class="tab-content" id="myTabContent">
       
        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <td>
                    <h5>Nama Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->nama_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>No KTP Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->no_ktp_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>No HP Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->no_hp_pembeli}}/h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Email Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->email_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Alamat Pembeli</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->alamat_pembeli}}</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Harga Tertagih</h5>
                  </td>
                  <td>
                    <h5>Rp {{$transaksis->harga_tertagih}},-</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5>Kode Transaksi</h5>
                  </td>
                  <td>
                    <h5>{{$transaksis->kode_transaksi}}</h5>
                  </td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
        <div class="tab-pane fade " id="review" role="tabpanel" aria-labelledby="review-tab">
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Produk</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Jumlah (Unit)</th>
                    <th scope="col">Total Harga</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($transaksis->produk as $produk)
                  @if($produk->produk)
                  @php
                   $totalHarga=0;
                   $totalHarga= $produk->jumlah_barang  * $produk->produk->harga;
                  @endphp
                  <tr>
                    <td>
                      <div class="media">
                        <div class="d-flex">
                            @if(sizeof($produk->produk->gambarProduk)>0)
                           
                                <img src="{{url('/')}}/aranoz/img/product/{{$produk->produk->gambarProduk[0]->link_file}}" alt="" height="120" width="100" />
                            @endif
                        </div>
                        <div class="media-body">
                            <p>{{$produk->produk->nama}}</p>
                        </div>
                      </div>
                    </td>
                    <td>
                      <h5>Rp {{$produk->produk->harga}},-</h5>
                      
                    </td>
                    <td>
                      <div class="product_count">
                        <h5> {{$produk->jumlah_barang}}</h5>
                      </div>
                    </td>
                    <td class="td-total-harga-{{$produk->id_barang}}">
                      <h5 class="total-harga-{{$produk->id_barang}}">Rp {{$totalHarga}},-</h5>
                    </td>
                  </tr>
                  @endif
                  @endforeach
                </tbody>
              </table>
              {{-- <div class="checkout_btn_inner float-right">
                <a class="btn_1" href="#">Continue Shopping</a>
                <a class="btn_1 checkout_btn_1" href="#">Proceed to checkout</a>
              </div> --}}
            </div>
      </div>
        
        
      </div>
    </div>
    <center>
      <div class="col-md-4">
        <a href="{{route('customer.transaction.create-payment').'?id='.$transaksis->kode_transaksi}}" class="genric-btn primary e-large">Lanjut Ke Pembayaran</a>
      </div>
    </center>
    
    
  </section>
@endsection
@push('styles')
<link rel="stylesheet" href="{{url('/')}}/aranoz/css/lightslider.min.css">
@endpush
@push('scripts')

  <script src="{{url('/')}}/aranoz/js/lightslider.min.js"></script>
  <script src="{{url('/')}}/aranoz/js/owl.carousel.min.js"></script>
  <script src="{{url('/')}}/aranoz/js/jquery.nice-select.min.js"></script>
  <script src="{{url('/')}}/aranoz/js/stellar.js"></script>
  
  <script src="{{url('/')}}/aranoz/js/theme.js"></script>
@endpush