<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductAndTransaksiSoftDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $arrTable=[
        'barang_x_transaksi',
        'pembayaran',
        'produk_x_gambar',
        'bukti_pembayaran',
        'users',
        'produk',
        'transaksi',
        
    ];
    public function up()
    {
        $arrTable=[
            'barang_x_transaksi',
            'pembayaran',
            'produk_x_gambar',
            'bukti_pembayaran',
            'users',
            'produk',
            'transaksi',
            
        ];
        foreach($arrTable as $tableName){
            if (!Schema::hasColumn($tableName, 'deleted_at')){
                Schema::table($tableName, function (Blueprint $table) {
                    $table->dateTime('deleted_at')->nullable();
                });
            }
        
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $arrTable=[
            'barang_x_transaksi',
            'pembayaran',
            'produk_x_gambar',
            'bukti_pembayaran',
            'users',
            'produk',
            'transaksi',
            
        ];
        foreach($arrTable as $tableName){
            if (Schema::hasColumn($tableName, 'deleted_at')){
                Schema::table($tableName, function (Blueprint $table) {
                    $table->dropColumn('deleted_at');
                });
            }
        
            
        }
    }
}
