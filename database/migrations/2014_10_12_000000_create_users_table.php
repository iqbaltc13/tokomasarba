<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName='users';
        if (Schema::hasTable($tableName)) {
            return 0;
        }
        
        Schema::create($tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->string('kode_otp_ganti_password', 10)->nullable()->default(NULL);
            $table->string('kode_otp_reset_password', 10)->nullable()->default(NULL);
            $table->string('token_firebase', 255)->nullable()->default(NULL);
            $table->string('token_login_mobile', 1000)->nullable()->default(NULL);
            $table->dateTime('token_login_mobile_kadaluarsa')->nullable()->default(NULL);
            $table->boolean('has_changed_password')->nullable()->default(FALSE);
            $table->string('foto')->nullable()->default(NULL);
            //$table->string('rfid_tid')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            //$table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
