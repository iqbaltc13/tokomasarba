<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGambarXBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName='produk_x_gambar';
        if (Schema::hasTable($tableName)) {
            return 0;
        }
        
        Schema::create($tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_produk')->nullable();
            $table->text('link_file')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
