<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName='produk';
        if (Schema::hasTable($tableName)) {
            return 0;
        }
        
        Schema::create($tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama')->nullable();
            $table->string('kategori')->nullable();
            $table->longText('keterangan')->nullable();
            $table->bigInteger('quantity')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });

        // $tableName='produk';
        // if (Schema::hasTable($tableName)) {
        //     return 0;
        // }
        
        // Schema::create($tableName, function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
