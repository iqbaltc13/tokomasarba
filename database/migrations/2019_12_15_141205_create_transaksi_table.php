<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName='transaksi';
        if (Schema::hasTable($tableName)) {
            return 0;
        }
        
        Schema::create($tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_pembeli')->nullable();
            $table->string('no_ktp_pembeli')->nullable();
            $table->string('no_hp_pembeli')->nullable();
            $table->string('alamat_pembeli')->nullable();
            $table->string('email_pembeli')->nullable();
            $table->bigInteger('harga_tertagih')->nullable();
            $table->string('kode_transaksi')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
