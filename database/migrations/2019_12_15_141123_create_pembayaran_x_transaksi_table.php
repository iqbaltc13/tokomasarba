<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranXTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName='pembayaran';
        if (Schema::hasTable($tableName)) {
            return 0;
        }
        
        Schema::create($tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_transaksi')->nullable();
            $table->string('nama_bank')->nullable();
            $table->string('no_rekening')->nullable();
            $table->string('rekening_atas_nama')->nullable();
            $table->string('jumlah_yang_ditransfer')->nullable();
            $table->dateTime('tanggal_transfer')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
