<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuktiPembayaranXPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName='bukti_pembayaran';
        if (Schema::hasTable($tableName)) {
            return 0;
        }
        
        Schema::create($tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pembayaran')->nullable();
            $table->text('link_bukti_bayar')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
