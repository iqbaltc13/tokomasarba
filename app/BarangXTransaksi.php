<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangXTransaksi extends Model
{
    use SoftDeletes;
    //
    protected $table = 'barang_x_transaksi';
    public $timestamps = true;
    protected $guarded=[];

    public function produk(){
		
		  return $this->belongsTo('App\Produk', 'id_barang','id')->withTrashed();
	
    }
    public function transaksi(){
		
		  return $this->belongsTo('App\Transaksi', 'id','id_transaksi')->withTrashed();
	
	  }
}
