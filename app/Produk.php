<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produk extends Model
{
    use SoftDeletes;
    protected $table = 'produk';
    public $timestamps = true;
    protected $guarded=[];

    public function gambarProduk(){
		
        return $this->hasMany('App\GambarProduk', 'id_produk','id');
  
    }
    
}
