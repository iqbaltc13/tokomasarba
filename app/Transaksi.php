<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaksi extends Model
{
    //
    use SoftDeletes;
    protected $table = 'transaksi';
    public $timestamps = true;
    protected $guarded=[];

    public function produk(){
		
        return $this->hasMany('App\BarangXTransaksi', 'id_transaksi','id')->withTrashed();
  
    }
    public function pembayaran(){
		
        return $this->hasOne('App\Pembayaran', 'id_transaksi','id')->withTrashed();
  
    }
    
}
