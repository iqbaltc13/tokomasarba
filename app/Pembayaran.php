<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pembayaran extends Model
{
    use SoftDeletes;
    protected $table = 'pembayaran';
    public $timestamps = true;
    protected $guarded=[];

    public function transaksi(){
		
        return $this->hasOne('App\Transaksi', 'id','id_transaksi')->withTrashed();
  
    }
    public function buktiPembayaran(){
		
        return $this->hasMany('App\BuktiPembayaran', 'id_pembayaran','id')->withTrashed();
  
    }
}
