<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GambarProduk extends Model
{
    use SoftDeletes;
    //
    protected $table = 'produk_x_gambar';
    public $timestamps = true;
    protected $guarded=[];

    public function produk(){
		
        return $this->belongsTo('App\Produk', 'id','id_produk')->withTrashed();
  
    }
}
