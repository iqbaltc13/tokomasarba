<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class UserTableSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateOrCreate();
    }
    private $map=[
        [
         '5113100018',
         'secret',
         'adi',
         'adipjb@gmailcom',
        ],
        [
            '5113100017',
            'secret',
            'budi',
            'budipjb@gmailcom',
        ],
    ];
    public function updateOrCreate(){

        $map=$this->map;
       
        for ($i=0; $i<sizeof($map) ; $i++) {
            
            User::updateOrCreate(
             ['email'=>$map[$i][3],
             ],
             [
                'nama'=>$map[$i][2],
                'password'=>bcrypt($map[$i][1]),
                'nip'=>$map[$i][0], 
             ]);

            
        }
    }
}
