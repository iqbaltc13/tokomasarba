<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BarangXTransaksi;
use App\BuktiPembayaran;
use App\GambarProduk;
use App\Pembayaran;
use App\Produk;
use App\Transaksi;
use App\User;
use DateTime;
use DateInterval;
use stdClass;
use DB;
use Illuminate\Support\Collection;
use Auth;
use Session;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->route='admin.profil.';
        $this->view='admin.profil.';
    }
    public function firstLanding(Request $request){
        $user=User::where('id',Auth::id())->first();
        $return=[
            'user'=>$user,
        ];
        return view('admin.first_landing.index',$return);
    }
    public function edit(Request $request){
        $method= $request->method();
        $id=$request->id;
        
        if($method=='POST'){
            $this->validate($request, [
                'nama'=>'required',
                'email'=>'required',
                
            ]);
            $arrUpdate=[
                'nama'=>$request->nama,
                'email'=>$request->email,
    
            ];
            $user=User::where('id',$id)->update($arrUpdate);
            return redirect()->route($this->route.'index');
        }
        elseif($method=='PUT'){
            $this->validate($request, [
                'nama'=>'required',
                'email'=>'required',
                
            ]);
            $arrUpdate=[
                'nama'=>$request->nama,
                'email'=>$request->email,
    
            ];
            $user=User::where('id',$id)->update($arrUpdate);
            return redirect()->route($this->route.'index');
        }
        else{
            $user=User::where('id',$id)->first();
            $arrParse=[
                'user'=>$user,
            ];
            return view($this->view.'edit',$arrParse);
        }
    }
    public function index(Request $request){
        $method= $request->method();
        return view($this->view.'index');
    }
    public function changePassword(Request $request){
        $method= $request->method();
        $id=$request->id;
        User::where('id',$id)->update([
            'password'=>bcrypt($request->password),
        ]);
        if($method=='POST'){
            $this->validate($request, [
                'password'=>'required',
                'confirm_password'=>'required|same:password',
                
            ]);
            $arrUpdate=[
                'nama'=>$request->nama,
                'email'=>$request->email,
    
            ];
            return redirect()->route($this->route.'index');
        }
        elseif($method=='PUT'){
            return redirect()->route($this->route.'index');
        }
        else{
            return view($this->view.'change-password');
        }
    }
}
