<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BarangXTransaksi;
use App\BuktiPembayaran;
use App\GambarProduk;
use App\Pembayaran;
use App\Produk;
use App\Transaksi;
use App\User;;
use DateTime;
use DateInterval;
use stdClass;
use DB;
use Illuminate\Support\Collection;
use Auth;
use Session;
use DataTables;

class TransaksiController extends Controller
{
    //
    public function __construct()
    {
        $this->route='admin.transaction.';
        $this->view='admin.transaction.';
    }
    public function data(Request $request){
        $model = Transaksi::with(['produk','pembayaran'])->orderBy('id','DESC');
        return DataTables::eloquent($model)
        // ->addColumn('created_at',function(Produk $produk){
        //     $return=$produk->created_at->format('Y-m-d H:i:s');
        //     return $return;

        // })
        ->make('true');
    }
    public function approve(Request $request){
        $method= $request->method();
        $id=$request->id_approve;
        Transaksi::where('id',$id)->update(['status'=>1]);
        return redirect()->route($this->route.'index');
       
    }
    public function delete(Request $request){
        $method= $request->method();
        $id=$request->id;
        Pembayaran::where('id_transaksi',$id)->delete();
        BarangXTransaksi::where('id_transaksi',$id)->delete();
        Transaksi::where('id',$id)->delete();
        return redirect()->route($this->route.'index');
       
    }
    public function index(Request $request){
        $method= $request->method();
        $transaksi=Transaksi::with(['produk','pembayaran'])->get();
        $arrParse=[
            'transaksis'=>$transaksi,
        ];
        return view($this->view.'index',$arrParse);
    }
    public function detail(Request $request){
        $method= $request->method();
        $id=$request->id;
        $transaksi=Transaksi::with(['produk','pembayaran'])->where('id',$id)->first();
        $arrParse=[
            'transaksis'=>$transaksi,
        ];
        return view($this->view.'detail',$arrParse);
    }
    public function changeCaraBayar(Request $request){
        $method= $request->method();
        $id=$request->id;
        if($method=='POST'){
            return redirect()->route($this->route.'index');
        }
        elseif($method=='PUT'){
            return redirect()->route($this->route.'index');
        }
        else{
            return view($this->view.'change-method-payment');
        }
    }
}
