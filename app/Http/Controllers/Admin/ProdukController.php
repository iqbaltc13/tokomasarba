<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BarangXTransaksi;
use App\BuktiPembayaran;
use App\GambarProduk;
use App\Pembayaran;
use App\Produk;
use App\Transaksi;
use App\User;;
use DateTime;
use DateInterval;
use stdClass;
use DB;
use Illuminate\Support\Collection;
use Auth;
use Session;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use DataTables;

class ProdukController extends Controller
{

    public function __construct()
    {
        $this->route='admin.product.';
        $this->view='admin.product.';
    }
    public function data(Request $request){
        $model = Produk::with(['gambarProduk'])
        ->orderBy('id','DESC');
        return DataTables::eloquent($model)
        // ->addColumn('created_at',function(Produk $produk){
        //     $return=$produk->created_at->format('Y-m-d H:i:s');
        //     return $return;

        // })
        ->make('true');
    }
    public function create(Request $request){
        $method= $request->method();
        if($method=='POST'){
            return $this->store($request);
        }
        
        else{
            return view($this->view.'create');
        }
        
    }
    
    public function store(Request $request){
        $method= $request->method();
        //dd($request);
        // dd($request->file('gambar_produk'));
        
        
        if($method=='POST'){
            $this->validate($request, [
            
                'nama'=>'required',
                'kategori'=>'required',
                'keterangan'=>'required',
                'quantity'=>'required',
                'harga'=>'required',
                
                
            ]);
            $arrCreateProduct=[
                'nama'=>$request->nama,
                'kategori'=>$request->kategori,
                'keterangan'=>$request->keterangan,
                'quantity'=>$request->quantity,
                'harga'=>$request->harga,
                'status'=>1,
            ];
            $createProduk=Produk::create($arrCreateProduct);
            //<input type="file" name="attachment[]" multiple>
            $fileGambar=$request->gambar_produk;
            //dd($fileGambar);
            
            $idProduk=$createProduk->id;
            
            foreach  ( $fileGambar as $key => $gambar) {
                $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, time().Auth::id());
                $name = $uuid;
                $ext = $gambar->extension();
                $path = '/aranoz/img/product/';
                $gambar->move(public_path($path), $name.'.'.$ext);
                $arrCreateGambarProduk=[
                    'id_produk'=>$idProduk,
                    'link_file'=>$name.'.'.$ext,
                    'status'=>1,
                ];
            
            
                $createGambarProduk = GambarProduk::create($arrCreateGambarProduk);
                # code...
            }
               
                    
                    
                
            
            return redirect()->route($this->route.'index');
        }
        
        else{
            return view($this->view.'create');
        }
    }

    public function index(Request $request){
        $method= $request->method();
        $produk=Produk::with(['gambarProduk'])->get();
        $arrParse=[
            'produks'=>$produk,
        ];
        return view($this->view.'index',$arrParse);
    }
    public function edit(Request $request){
        $method= $request->method();
        $produk=Produk::with(['gambarProduk'])->where('id',$request->id)->first();
        
        if($method=='POST'){
            return $this->update($request);
        }
        elseif($method=='PUT'){
            return $this->update($request);
        }
        else{
            $arrParse=[
                'produks'=>$produk,
            ];

            return view($this->view.'edit',$arrParse);
        }
    }
    public function update(Request $request){
        $method= $request->method();
        $id=$request->id;
        //dd($fileGambar=$request->gambar_produk);
        $this->validate($request, [
            
            'nama'=>'required',
            'kategori'=>'required',
            'keterangan'=>'required',
            'quantity'=>'required',
            'harga'=>'required',
            
            
        ]);
        $arrChange=[
                'nama'=>$request->nama,
                'kategori'=>$request->kategori,
                'keterangan'=>$request->keterangan,
                'quantity'=>$request->quantity,
                'harga'=>$request->harga,
                'status'=>1,
        ];
        
        $update=Produk::where('id',$id)->update($arrChange);
        $fileGambar=$request->gambar_produk;
            //dd($fileGambar);
            
        $idProduk=$id;
        if(!is_null($fileGambar)){
            if(sizeof($fileGambar) > 0){
                GambarProduk::where('id_produk',$idProduk)->delete();
                foreach  ( $fileGambar as $key => $gambar) {
                    $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, time().Auth::id());
                    $name = $uuid;
                    $ext = $gambar->extension();
                    $path = '/aranoz/img/product/';
                    $gambar->move(public_path($path), $name.'.'.$ext);
                    $arrCreateGambarProduk=[
                        'id_produk'=>$idProduk,
                        'link_file'=>$name.'.'.$ext,
                        'status'=>1,
                    ];
                
                
                    $createGambarProduk = GambarProduk::create($arrCreateGambarProduk);
                    # code...
                }
            }
        }
       
        
            
        
        
        return redirect()->route($this->route.'index');
        
        
    }
    public function delete(Request $request){
        $method= $request->method();
        $id=$request->id;
        GambarProduk::where('id_produk',$id)->delete();
        Produk::where('id',$id)->delete();
        return redirect()->route($this->route.'index');
        
        
        
           
        
    }
    public function detail(Request $request){
        $method= $request->method();
        $id=$request->id;
        $produk=Produk::with(['gambarProduk'])->where('id',$request->id)->first();
        $arrParse=[
                'produks'=>$produk,
            ];
        return view($this->view.'detail',$arrParse);
    }
    public function detailJson(Request $request){
        $method= $request->method();
        $id=$request->id;
        $produk=Produk::with(['gambarProduk'])->where('id',$request->id)->first();
        $arrParse=[
            'response_code'=>200,
            'error_message'=>0,
            'data'=>$produk,
        ];
        return response()->json($arrParse);
    }
}
