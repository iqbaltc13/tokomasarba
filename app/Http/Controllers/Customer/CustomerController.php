<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BarangXTransaksi;
use App\BuktiPembayaran;
use App\GambarProduk;
use App\Pembayaran;
use App\Produk;
use App\Transaksi;
use App\User;;
use DateTime;
use DateInterval;
use stdClass;
use DB;
use Illuminate\Support\Collection;

class CustomerController extends Controller
{
    //
    public function __construct()
    {
        $this->route='customer.product.';
        $this->view='customer.product.';
    }
    public function index(Request $request){
        $method= $request->method();
        $produk=Produk::with(['gambarProduk'])->get();
        $arrParse=[
            'produks'=>$produk,
        ];
        return view($this->view.'index',$arrParse);
    }
    public function detail(Request $request){
        $method= $request->method();
        $id=$request->id;
        $produk=Produk::with(['gambarProduk'])->where('id',$request->id)->first();
        $arrParse=[
                'produks'=>$produk,
            ];
        return view($this->view.'detail',$arrParse);
    }
    public function detailJson(Request $request){
        $method= $request->method();
        $id=$request->id;
        $produk=Produk::with(['gambarProduk'])->where('id',$request->id)->first();
        $arrParse=[
            'response_code'=>200,
            'error_message'=>0,
            'data'=>$produk,
        ];
        return response()->json($arrParse);
    }
}
