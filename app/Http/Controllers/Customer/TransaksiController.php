<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BarangXTransaksi;
use App\BuktiPembayaran;
use App\GambarProduk;
use App\Pembayaran;
use App\Produk;
use App\Transaksi;
use App\User;;
use DateTime;
use DateInterval;
use stdClass;
use DB;
use Illuminate\Support\Collection;
use Redirect;

class TransaksiController extends Controller
{
    public function __construct()
    {
        $this->route='customer.transaction.';
        $this->view='customer.transaction.';
    }
    public function store(Request $request){

        $this->validate($request, [
            
            
            'nama_pembeli'=>'required',
            'no_ktp_pembeli'=>'required',
            'no_hp_pembeli'=>'required',
            'alamat_pembeli'=>'required',
            'email_pembeli'=>'required',
            
            
        ]);
        $dateTime= new DateTime();
        $arrCreate=[
            'nama_pembeli'=>$request->nama_pembeli,
            'no_ktp_pembeli'=>$request->no_ktp_pembeli,
            'no_hp_pembeli'=>$request->no_hp_pembeli,
            'alamat_pembeli'=>$request->alamat_pembeli,
            'email_pembeli'=>$request->email_pembeli,
            'harga_tertagih'=>$request->harga_tertagih,
            'kode_transaksi'=>bcrypt($request->no_ktp_pembeli.$dateTime->format('Y-m-d H:i:s')),
            'status'=>0,
        ];
        $createTransaksi=Transaksi::create($arrCreate);
        $produks=$request->id_produk;
        $jumlah=$request->quantity_produk;
        $idTransaksi=$createTransaksi->id;
        for ($i=0; $i <sizeof($produks) ; $i++) { 
            $arrCreateBarangXTransaksi=[
                'id_barang'=>$produks[$i],
                'id_transaksi'=>$idTransaksi,
                'jumlah_barang'=>$jumlah[$i],
            ];
           $createTransaksi=BarangXTransaksi::create($arrCreateBarangXTransaksi);
        }
        $url=route("customer.transaction.detail");
        $url=$url.'?id='.$idTransaksi;
        return Redirect::to($url);
    }
    public function detail(Request $request){
        $id=$request->id;
        $transaksi=Transaksi::with(['produk','pembayaran'])->where('id',$id)->first();
        $arrParse=[
            'transaksis'=>$transaksi,
        ];
        return view($this->view.'detail',$arrParse);
    }
    public function detailJson(Request $request){
        $id=$request->id;
        $transaksi=Transaksi::with(['produk','pembayaran'])->where('id',$id)->first();
        $arrParse=[
            'response_code'=>200,
            'error_message'=>0,
            'data'=>$transaksi,
        ];
        return response()->json($arrParse);
    }
    public function createPembayaran(Request $request){
        $id=$request->id;
        $transaksi=Transaksi::with(['produk','pembayaran'])->where('kode_transaksi',$id)->first();
        //dd($transaksi);
        $arrParse=[
            'transaksis'=>$transaksi,
        ];

        return view($this->view.'create-payment',$arrParse);
    }
    public function storePembayaran(Request $request){
        $this->validate($request, [
            
            
            'nama_bank'=>'required',
            'no_rekening'=>'required',
            'rekening_atas_nama'=>'required',
            'jumlah_yang_ditransfer'=>'required',
            'tanggal_transfer'=>'required',
            
            
        ]);
        $arrCreate=[
            'id_transaksi'=>$request->id_transaksi,
            'nama_bank'=>$request->nama_bank,
            'no_rekening'=>$request->no_rekening,
            'rekening_atas_nama'=>$request->rekening_atas_nama,
            'jumlah_yang_ditransfer'=>$request->jumlah_yang_ditransfer,
            'tanggal_transfer'=>$request->tanggal_transfer,
            'status'=>0,
        ];
        $createPembayaran=Pembayaran::create($arrCreate);
        $file = $request->file('bukti_transaksi');
        if($file){
            $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, time().$request->no_rekening);
            $name = $uuid;
            $ext = $file->extension();
            $path = '/uploads/'.'Bukti Transaksi'.'/';
            $file->move(public_path($path), $name.'.'.$ext);
            $arrCreateBuktiPembayaran=[
            
                'id_pembayaran'=>$createPembayaran->id,
                'link_bukti_bayar'=>$link,
                'status'=>0,
            ];
            $createBuktiPembayaran=BuktiPembayaran::create($arrCreateBuktiPembayaran);

        }
            
        $url=route($this->route."view-after-payment");
        $url=$url.'?id='.$request->id_transaksi;
        return Redirect::to($url);
        
    }
    public function viewAfterPembayaran(Request $request){
        $id=$request->id;
        $transaksi=Transaksi::with(['produk','pembayaran'])->where('id',$id)->first();
        $arrParse=[
            'transaksis'=>$transaksi,
        ];
        return view($this->view.'view-after-payment',$arrParse);
    }
    public function create(Request $request){

        $method= $request->method();
        //if($method=='POST'){
        $arrIdProduk=$request->id_produk;
        $produk=Produk::with(['gambarProduk'])->whereIn('id',$arrIdProduk)->get();
        $arrParse=[
            'produks'=>$produk,
        ];
        return view($this->view.'create-transaction',$arrParse);
        // }
        // else{
        //     return redirect()->route($this->route.'customer.product.index');
        // }
        

    }
}
