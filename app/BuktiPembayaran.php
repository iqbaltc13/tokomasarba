<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuktiPembayaran extends Model
{
    use SoftDeletes;
    //
    protected $table = 'bukti_pembayaran';
    public $timestamps = true;
    protected $guarded=[];

    public function pembayaran(){
		
		return $this->belongsTo('App\Pembayaran', 'id','id_pembayaran')->withTrashed();
	
    }
}
